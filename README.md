# Nginx role

A role for deploying nginx servers and proxies. Much of the configuration for this role follows the same names and syntax as the nginx configuration. See the [Nginx documentation](https://docs.nginx.com/) for more information.

Example vars:

```yaml
nginx:
  worker_processes: 5
  worker_connections: 1024
  redirect_http: true
  servers:
    - names:
        - example.com
      addresses:
        - 443 ssl
        - 127.0.0.1:80
      ssl:
        cert_file: /usr/local/etc/ssl/example.com.cert
        key_file: /usr/local/etc/ssl/example.com.key
      root: /usr/local/www/example.com/public
      upstream_archive:
        url: https://ci.example.com/build/www
        extract_to: /usr/local/www/example.com
    - names:
        - example.org
      addresses:
        - 443 ssl
      root: /usr/local/www/example.org
      ssl:
        cert_file: /usr/local/etc/ssl/example.org.cert
        key_file: /usr/local/etc/ssl/example.org.key
    - names:
        - example.net
      addresses:
        - 443 ssl
      locations:
        - name: "/"
          proxy_pass: https://example.org
```

- `nginx` - The nginx dictionary contains configuration for all nginx servers on this host it descends into the following subdictionaries:
  - `server_names_hash_bucket_size`: Same field as in nginx configuration. Defaults to `64`.
  - `worker_processes`: Same field as in nginx configuration. Defaults to `5`.
  - `worker_connections`: Same field as in nginx configuration. Defaults to `1025`.
  - `redirect_http`: Boolean - Adds a server block to the nginx configuration that redirects all incoming traffic to port 80 to port 443. Defaults to `true`.
  - `upstream_archive`: (Optional) A dictionary containing details of a remote archive that is pulled and extracted to either the `root` by default or `extract_to` if specified. Consists of the following:
    - `url`: The upstream url to download the archive from - (e.g. The artifacts of a CI job)
    - `extract_to`: The path to extract the archive to
  -`servers`: A list of dictionaries, each of which configures a `server` in the nginx configuration:
    - `names`: (Optional) The list of `server_names` of this server
    - `addresses`: List - The addresses to listen, same syntax as nginx configuration.
    - `allows`: (Optional) List - The hosts to allow access. See [nginx docs] for allowed values.
    - `denies`: (Optional) List - The hosts to deny access. See [nginx docs] for allowed values.
    - `ssl` (Optional) Dict
      - `cert_file`: The path to this SSL cert
      - `key_file`: The path to the key for this SSL cert
    - `root`: (Optional) The path to the root of this server
    - `gzip`: (Optional) Determines whether gzip compression is enabled on this server. Defaults to `false`.
    - `tokens`: (Optional) Determines whether nginx includes the server and OS information (such as version) in the HTTP header. Defaults to `true`.
    - `gzip_types`: (Optional) [List] Defines which MIME types should be compressed in responses. Defaults to `['text/html']`.
    - `gzip_min_length`: (Optional) Specifies the minimum length of the responses to compress in bytes. Defaults to `20`.
    - `locations`: (Optional) List of dictionaries - locations to add to the server in the nginx configuration:
      - `autoindex`: Boolean - Determines whether autoindexing is enabled
      - `name`: The 'name' of the location e.g. '/'
      - `proxy_pass`: Same field as in nginx configuration
      - `proxy_bind`: Same field as in nginx configuration
      - `proxy_http_version`: Same field as in nginx configuration
      - `proxy_set_headers`: A dictionary whose subdictionaries are key-value pairs as per the [Nginx docs](https://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_set_header) for this parameter.
      - `root`: Root of this location

[nginx docs]: nginx.org/en/docs/http/ngx_http_access_module.html

## PHP Support

This ansible role can also be used to configure [php-fpm](https://www.php.net/manual/en/install.fpm.php) on the target server. It has php-fpm listen on a _unix socket_ and configures nginx to proxy php requests to php-fpm.

The `php_version` variable dictates the version of php that is installed on the targets - it defaults to `82`.

### php-fpm configuration

- `php_fpm_conf_dir`: Path to the `php-fpm.d` dir where configuration is templated. Defaults to `/etc/php-fpm.d` on Linux and `/usr/local/etc/php-fpm.d` on FreeBSD.
- `php_fpm_socket_path`: The path to the unix socket where php-fpm will listen for requests. Defaults to `/tmp/php-fpm.sock`. This is also used to configure `fastcgi_pass` in the nginx configuration.
- `php_fpm_owner`: The owner of the php-fpm process. Defaults to `www`.
- `php_fpm_pm`: Choose how the process manager will control the number of child processes. Defaults to `dynamic`.
- `php_fpm_pm_max_children`: The maximum number of children that can be alive at the same time when mode is 'dynamic'. Defaults to `5`.
- `php_fpm_pm_start_servers`: The number of children created on startup when mode is 'dynamic'. Defaults to `2`.
- `php_fpm_pm_min_spare_servers`: The minimum number of children in 'idle' state (waiting to process). If the number of 'idle' processes is less than this then some children will be created. Only for 'dynamic' mode. Defaults to `1`.
- `php_fpm_pm_max_spare_servers`: The maximum number of children in 'idle' state (waiting to process). If the number of 'idle' processes is greater than this then some children will be destroyed. Only for 'dynamic' mode. Defaults to `2`.

### Nginx configuration

The fastcgi php support is configured _per server_ via the `php` dictionary:

- `php`: This dictionary descends into other parameters used to configure php per-server, but the mere presence of the `php` dict will create a `location ~ \.php$` block in the nginx server configuration at set `fastcgi_pass`
  - `fastcgi_index`: The file that is appended to `SCRIPT_FILENAME` is the request URI ends in a slash (/)
  - `fastcgi_params`: This descends into key/value pairs for each `fastcgi_param` that should be configured on this server.

### PHP Example

```yaml
nginx:
  servers:
    - names:
        - pla.smoothbrained.co.uk
      index_files:
        - index.php
      php:
        fastcgi_index: index.php
        fastcgi_params:
          SCRIPT_FILENAME: "$document_root/$fastcgi_script_name"
      addresses:
        - 80
      root: /usr/local/www/phpldapadmin/htdocs
```
